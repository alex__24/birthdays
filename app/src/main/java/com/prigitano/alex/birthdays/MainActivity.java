package com.prigitano.alex.birthdays;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.prigitano.alex.birthdays.fragments.MainFragment;
import com.prigitano.alex.birthdays.model.Birthday;
import com.prigitano.alex.birthdays.model.BirthdayViewModel;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(null == savedInstanceState) {
            MainFragment fragment = new MainFragment();
            changeFragment(fragment, false);
        }
    }

    public void changeFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment, ((Object) fragment).getClass().getSimpleName());
        if (addToBackStack)
            ft.addToBackStack("");
        ft.commit();
    }
}
