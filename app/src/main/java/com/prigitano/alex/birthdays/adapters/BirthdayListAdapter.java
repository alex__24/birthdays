package com.prigitano.alex.birthdays.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.prigitano.alex.birthdays.R;
import com.prigitano.alex.birthdays.model.Birthday;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class BirthdayListAdapter extends RecyclerView.Adapter<BirthdayListAdapter.BirthdayViewHolder> {

    class BirthdayViewHolder extends RecyclerView.ViewHolder {
        private final TextView nameItemView;
        private final TextView dateItemView;

        private BirthdayViewHolder(View itemView) {
            super(itemView);
            nameItemView = itemView.findViewById(R.id.nameTextView);
            dateItemView = itemView.findViewById(R.id.dateTextView);
        }
    }

    private final LayoutInflater mInflater;
    private List<Birthday> birthdays;

    public BirthdayListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public BirthdayViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.birthday_item, parent, false);
        return new BirthdayViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(BirthdayViewHolder holder, int position) {
        Birthday current = birthdays.get(position);
        holder.nameItemView.setText(current.getName());
        String currentDateTimeString = DateFormat.getDateInstance().format(current.getDate());
        holder.dateItemView.setText(currentDateTimeString);
    }

    public void setBirthdays(List<Birthday> birthdays) {
        this.birthdays = birthdays;
        final Calendar calNow = Calendar.getInstance();
        calNow.set(Calendar.HOUR_OF_DAY, 0);
        calNow.set(Calendar.MINUTE, 0);
        calNow.set(Calendar.SECOND, 0);
        calNow.set(Calendar.MILLISECOND, 0);

        Collections.sort(this.birthdays, new Comparator<Birthday>() {
            @Override
            public int compare(Birthday b1, Birthday b2) {
                Calendar cal1 = Calendar.getInstance();
                cal1.setTime(b1.getDate());
                cal1.set(Calendar.YEAR, calNow.get(Calendar.YEAR));
                cal1.set(Calendar.HOUR_OF_DAY, 0);
                cal1.set(Calendar.MINUTE, 0);
                cal1.set(Calendar.SECOND, 0);
                cal1.set(Calendar.MILLISECOND, 0);

                Calendar cal2 = Calendar.getInstance();
                cal2.setTime(b2.getDate());
                cal2.set(Calendar.YEAR, calNow.get(Calendar.YEAR));
                cal2.set(Calendar.HOUR_OF_DAY, 0);
                cal2.set(Calendar.MINUTE, 0);
                cal2.set(Calendar.SECOND, 0);
                cal2.set(Calendar.MILLISECOND, 0);

                if (cal1.getTime().after(calNow.getTime()) && cal2.getTime().after(calNow.getTime())) {
                    return cal1.getTime().compareTo(cal2.getTime());
                }
                if (cal1.getTime().before(calNow.getTime()) && cal2.getTime().before(calNow.getTime())) {
                    return cal1.getTime().compareTo(cal2.getTime());
                }
                if(cal1.getTimeInMillis() == calNow.getTimeInMillis()){ //if today
                    return -1;
                }
                if(cal2.getTimeInMillis() == calNow.getTimeInMillis()){ //if today
                    return 1;
                }
                return -cal1.getTime().compareTo(cal2.getTime());
            }
        });
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (birthdays != null)
            return birthdays.size();
        else return 0;
    }
}
