package com.prigitano.alex.birthdays.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.prigitano.alex.birthdays.model.Birthday;

import java.util.List;

@Dao
public interface BirthdayDao {

    @Query("SELECT * FROM birthdays")
    LiveData<List<Birthday>> getBirthdays();

    @Insert
    void insert(Birthday birthday);


    @Query("DELETE FROM birthdays")
    void deleteAll();
}
