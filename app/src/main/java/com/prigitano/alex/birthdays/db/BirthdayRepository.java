package com.prigitano.alex.birthdays.db;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.prigitano.alex.birthdays.model.Birthday;

import java.util.List;

public class BirthdayRepository {
    private BirthdayDao birthdayDao;
    private LiveData<List<Birthday>> birthdays;

    public BirthdayRepository(Application application) {
        BirthdayRoomDatabase db = BirthdayRoomDatabase.getDatabase(application);
        birthdayDao = db.birthdayDao();
        birthdays = birthdayDao.getBirthdays();
    }

    public LiveData<List<Birthday>> getBirthdays() {
        return birthdays;
    }

    public void insert (Birthday birthday) {
        new InsertAsyncTask(birthdayDao).execute(birthday);
    }

    private static class InsertAsyncTask extends AsyncTask<Birthday, Void, Void> {

        private BirthdayDao mAsyncTaskDao;

        InsertAsyncTask(BirthdayDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Birthday... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    public void deleteAll () {
        new DeleteAllAsyncTask(birthdayDao).execute();
    }

    private static class DeleteAllAsyncTask extends AsyncTask<Void, Void, Void> {

        private BirthdayDao mAsyncTaskDao;

        DeleteAllAsyncTask(BirthdayDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mAsyncTaskDao.deleteAll();
            return null;
        }
    }
}
