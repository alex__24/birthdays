package com.prigitano.alex.birthdays.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.prigitano.alex.birthdays.model.Birthday;

import java.util.Calendar;

@Database(entities = {Birthday.class}, version = 1)
public abstract class BirthdayRoomDatabase extends RoomDatabase {

    public abstract BirthdayDao birthdayDao();

    private static BirthdayRoomDatabase INSTANCE;

    static BirthdayRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (BirthdayRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            BirthdayRoomDatabase.class, "birthdays_database")
                            // Wipes and rebuilds instead of migrating if no Migration object.
                            .fallbackToDestructiveMigration()
//                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }
    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback(){

        @Override
        public void onOpen (@NonNull SupportSQLiteDatabase db){
            super.onOpen(db);
            new PopulateDbAsync(INSTANCE).execute();
        }
    };


    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final BirthdayDao dao;

        PopulateDbAsync(BirthdayRoomDatabase db) {
            dao = db.birthdayDao();
        }

        @Override
        protected Void doInBackground(final Void... params) {
            dao.deleteAll();

            Birthday birthday = new Birthday();
            birthday.setName("Test1");
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, 1990);
            cal.set(Calendar.MONTH, 3);
            cal.set(Calendar.DAY_OF_MONTH, 24);
            birthday.setDate(cal.getTime());
            dao.insert(birthday);

            birthday = new Birthday();
            birthday.setName("Test2");
            cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, 1987);
            cal.set(Calendar.MONTH, 6);
            cal.set(Calendar.DAY_OF_MONTH, 17);
            birthday.setDate(cal.getTime());
            dao.insert(birthday);

            birthday = new Birthday();
            birthday.setName("Test3");
            cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, 1995);
            cal.set(Calendar.MONTH, 11);
            cal.set(Calendar.DAY_OF_MONTH, 17);
            birthday.setDate(cal.getTime());
            dao.insert(birthday);

            birthday = new Birthday();
            birthday.setName("Test4");
            cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, 1987);
            cal.set(Calendar.MONTH, 04);
            cal.set(Calendar.DAY_OF_MONTH, 05);
            birthday.setDate(cal.getTime());
            dao.insert(birthday);

            birthday = new Birthday();
            birthday.setName("Test5");
            cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, 1998);
            cal.set(Calendar.MONTH, 1);
            cal.set(Calendar.DAY_OF_MONTH, 2);
            birthday.setDate(cal.getTime());
            dao.insert(birthday);

            birthday = new Birthday();
            birthday.setName("Test6");
            cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, 1998);
            cal.set(Calendar.MONTH, 1);
            cal.set(Calendar.DAY_OF_MONTH, 22);
            birthday.setDate(cal.getTime());
            dao.insert(birthday);

            birthday = new Birthday();
            birthday.setName("Test7");
            cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, 1965);
            cal.set(Calendar.MONTH, 8);
            cal.set(Calendar.DAY_OF_MONTH, 4);
            birthday.setDate(cal.getTime());
            dao.insert(birthday);

            birthday = new Birthday();
            birthday.setName("Test8");
            cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, 1985);
            cal.set(Calendar.MONTH, 4);
            cal.set(Calendar.DAY_OF_MONTH, 3);
            birthday.setDate(cal.getTime());
            dao.insert(birthday);

            birthday = new Birthday();
            birthday.setName("Test9");
            cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, 1985);
            cal.set(Calendar.MONTH, 4);
            cal.set(Calendar.DAY_OF_MONTH, 6);
            birthday.setDate(cal.getTime());
            dao.insert(birthday);

            birthday = new Birthday();
            birthday.setName("Test10");
            cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, 1971);
            cal.set(Calendar.MONTH, 9);
            cal.set(Calendar.DAY_OF_MONTH, 3);
            birthday.setDate(cal.getTime());
            dao.insert(birthday);
            return null;
        }
    }
}