package com.prigitano.alex.birthdays.fragments;

import android.Manifest;
import android.app.DatePickerDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.prigitano.alex.birthdays.R;
import com.prigitano.alex.birthdays.model.Birthday;
import com.prigitano.alex.birthdays.model.BirthdayViewModel;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * A placeholder fragment containing a simple view.
 */
public class AddBirthdayFragment extends Fragment {
    private final int PERMISSIONS_RECORD_AUDIO = 1;
    private BirthdayViewModel birthdayViewModel;
    private ConstraintLayout addBirthdayConstraintLayout;
    private EditText editTextName, editTextDate;
    private CheckBox checkBoxAddToCalendar;
    private TextInputLayout textInputLayoutName, textInputLayoutDate;
    private SpeechRecognizer mSpeechRecognizer;
    private boolean mIsListening;
    private Intent mSpeechRecognizerIntent;
    private RecognitionListener speechRecognitionListener = new RecognitionListener() {

        @Override
        public void onBeginningOfSpeech() {
            Log.d("Speech", "onBeginingOfSpeech");
        }

        @Override
        public void onBufferReceived(byte[] buffer) {

        }

        @Override
        public void onEndOfSpeech() {
            Log.d("Speech", "onEndOfSpeech");
        }

        @Override
        public void onError(int error) {
            Log.d("Speech", "error = " + error);
        }

        @Override
        public void onEvent(int eventType, Bundle params) {

        }

        @Override
        public void onPartialResults(Bundle partialResults) {

        }

        @Override
        public void onReadyForSpeech(Bundle params) {
            Log.d("Speech", "onReadyForSpeech");
        }

        @Override
        public void onResults(Bundle results) {
            Log.d("Speech", "onResults");
            ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
//            StringBuilder stringBuilder = new StringBuilder();
//            for (String m : matches) {
//                Log.d("Speech", m);
//                stringBuilder.append(m);
//            }
            if (matches.size() > 0)
                editTextName.setText(matches.get(0));
        }

        @Override
        public void onRmsChanged(float rmsdB) {
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        birthdayViewModel = ViewModelProviders.of(this).get(BirthdayViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_add_birthday, container, false);
        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(getActivity());
        mSpeechRecognizer.setRecognitionListener(speechRecognitionListener);
        mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getActivity().getPackageName());

        addBirthdayConstraintLayout = view.findViewById(R.id.addBirthdayConstraintLayout);
        editTextName = view.findViewById(R.id.editTextName);
        editTextDate = view.findViewById(R.id.editTextDate);
        textInputLayoutName = view.findViewById(R.id.textInputLayoutName);
        textInputLayoutDate = view.findViewById(R.id.textInputLayoutDate);
        checkBoxAddToCalendar = view.findViewById(R.id.checkBoxAddToCalendar);

        editTextName.addTextChangedListener(new MyTextWatcher(editTextName));
//        editTextDate.addTextChangedListener(new MyTextWatcher(editTextDate));
        editTextDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard(editTextName);
                Calendar cal = Calendar.getInstance();
                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        getActivity(), onDateSetListener, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.setOnCancelListener(onCancelListener);
                datePickerDialog.setCancelable(true);
                datePickerDialog.show();
            }
        });
        ImageButton speechNameButton = view.findViewById(R.id.speechNameButton);
        speechNameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mIsListening) {
                    requestAudioPermissions();
                }
            }
        });
        Button addButton = view.findViewById(R.id.addButton);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateField(editTextName, textInputLayoutName) && validateField(editTextDate, textInputLayoutDate)) {
                    String nameString = editTextName.getText().toString();
                    String dateString = editTextDate.getText().toString();

                    DateFormat df = DateFormat.getDateInstance();
                    Calendar cal = Calendar.getInstance();
                    try {
                        cal.setTime(df.parse(dateString));
                    } catch (ParseException e) {
                        e.printStackTrace();
                        return;
                    }
                    Birthday birthday = new Birthday();
                    birthday.setName(nameString);
                    birthday.setDate(cal.getTime());
                    birthdayViewModel.insert(birthday);
                    if (checkBoxAddToCalendar.isChecked()) {
                        Intent intent = new Intent(Intent.ACTION_INSERT);
                        intent.setType("vnd.android.cursor.item/event");
                        StringBuilder eventName = new StringBuilder(getActivity().getString(R.string.birthday))
                                .append(" ")
                                .append(nameString);
                        intent.putExtra(CalendarContract.Events.TITLE, eventName.toString());
                        intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,
                                cal.getTimeInMillis());
                        intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME,
                                cal.getTimeInMillis());
                        intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, true);

                        startActivity(intent);
                    }
                    hideSoftKeyboard(editTextName);
                    getActivity().onBackPressed();
                }
            }
        });
        addBirthdayConstraintLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideSoftKeyboard(editTextName);
                return false;
            }
        });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mSpeechRecognizer != null) {
            mSpeechRecognizer.destroy();
        }
        hideSoftKeyboard(editTextName);
    }

    private void requestAudioPermissions() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {

            //When permission is not granted by user, show them message why this permission is needed.
            if (shouldShowRequestPermissionRationale(
                    Manifest.permission.RECORD_AUDIO)) {
                Toast.makeText(getActivity(), getActivity().getString(R.string.grant_permissions), Toast.LENGTH_LONG).show();

                //Give user option to still opt-in the permissions
                requestPermissions(
                        new String[]{Manifest.permission.RECORD_AUDIO}, PERMISSIONS_RECORD_AUDIO);

            } else {
                // Show user dialog to grant permission to record audio
                requestPermissions(
                        new String[]{Manifest.permission.RECORD_AUDIO}, PERMISSIONS_RECORD_AUDIO);
            }
        }
        //If permission is granted, then go ahead recording audio
        else if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.RECORD_AUDIO)
                == PackageManager.PERMISSION_GRANTED) {

            //Go ahead with recording audio now
            mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
        }
    }

    //Handling callback
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_RECORD_AUDIO: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay!
                    mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(getActivity(), getActivity().getString(R.string.permissions_denied), Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    private void hideSoftKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
        addBirthdayConstraintLayout.requestFocus();
    }

    private boolean validateField(EditText editText, TextInputLayout textInputLayout) {
        if (editText.getId() == editTextName.getId() && editText.getText().toString().trim().isEmpty()) {
            textInputLayout.setError(getString(R.string.err_msg_name));
            requestFocus(editText);
            return false;
        } else if (editText.getId() == editTextDate.getId()) {
            if (!editText.getText().toString().trim().isEmpty()) {
                DateFormat df = DateFormat.getDateInstance();
                try {
                    df.parse(editText.getText().toString());
                    return true;
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            textInputLayout.setError(getString(R.string.err_msg_date));
            requestFocus(editText);
            return false;
        } else {
            textInputLayout.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private DatePickerDialog.OnCancelListener onCancelListener = new DatePickerDialog.OnCancelListener() {

        @Override
        public void onCancel(DialogInterface dialog) {
            hideSoftKeyboard(editTextDate);
        }
    };
    private DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, year);
            cal.set(Calendar.MONTH, month);
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            editTextDate.setText(DateFormat.getDateInstance().format(cal.getTime()));
            hideSoftKeyboard(editTextDate);
        }
    };

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.editTextName:
                    validateField(editTextName, textInputLayoutName);
                    break;
//                case R.id.editTextDate:
//                    validateField(editTextDate, textInputLayoutDate);
//                    break;
            }
        }
    }
}
