package com.prigitano.alex.birthdays.fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.prigitano.alex.birthdays.MainActivity;
import com.prigitano.alex.birthdays.R;
import com.prigitano.alex.birthdays.adapters.BirthdayListAdapter;
import com.prigitano.alex.birthdays.model.Birthday;
import com.prigitano.alex.birthdays.model.BirthdayViewModel;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainFragment extends Fragment {
    private BirthdayViewModel birthdayViewModel;
    private BirthdayListAdapter adapter;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        birthdayViewModel = ViewModelProviders.of(this).get(BirthdayViewModel.class);
        birthdayViewModel.getBirthdays().observe(this, new Observer<List<Birthday>>() {
            @Override
            public void onChanged(@Nullable final List<Birthday> birthdays) {
                adapter.setBirthdays(birthdays);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_main, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        adapter = new BirthdayListAdapter(getActivity());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        DividerItemDecoration div = new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL);
        div.setDrawable(getActivity().getResources().getDrawable(R.drawable.list_item_divider));
        recyclerView.addItemDecoration(div);
        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddBirthdayFragment fragment = new AddBirthdayFragment();
                ((MainActivity)getActivity()).changeFragment(fragment, true);
            }
        });

        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_delete_all) {
            birthdayViewModel.deleteAll();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
