package com.prigitano.alex.birthdays.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.prigitano.alex.birthdays.utils.DateConverter;

import java.util.Collections;
import java.util.Date;

@Entity(tableName = "birthdays")
@TypeConverters(DateConverter.class)
public class Birthday {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    @NonNull
    private int id;

    @ColumnInfo(name = "date")
    @NonNull
    private Date date;

    @ColumnInfo(name = "name")
    private String name;


    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(@NonNull Date date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }
}