package com.prigitano.alex.birthdays.model;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;

import com.prigitano.alex.birthdays.db.BirthdayRepository;

import java.util.List;

public class BirthdayViewModel extends AndroidViewModel {

    private BirthdayRepository repository;
    private LiveData<List<Birthday>> birthdays;
    public BirthdayViewModel(Application application) {
        super(application);
        repository = new BirthdayRepository(application);
        birthdays = repository.getBirthdays();
    }

    public LiveData<List<Birthday>> getBirthdays() {
        return birthdays;
    }

    public void insert(Birthday birthday) {
        repository.insert(birthday);
    }


    public void deleteAll() {
        repository.deleteAll();
    }
}